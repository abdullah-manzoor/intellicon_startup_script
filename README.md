## Intellicon_startup_script

## For download this script you need to access on the repo....

## For the pull you need to be install git on you system with the following command:
yum install git -y

## Update the System with the following command and reboot to avoid any inconvenience
yum update -y && reboot

## After this download the kick_start script and run the following commands:
chmod 755 kick_start
./kick_start

## Follow the instraction according to startup script.

## Add the IP address on which you need to run the Agent Panel: Run the following commands:
sed -i -e 's/IPADDRESS/{Replace this with IP address}/g' /etc/rtpengine/rtpengine.conf 
sed -i -e 's/IPADDRESS/{Replace this with IP address}/g' /etc/opensips/opensips.cfg 

## Example: sed -i -e 's/IPADDRESS/192.168.0.10/g' /etc/rtpengine/rtpengine.conf
## Example: sed -i -e 's/IPADDRESS/192.168.0.10/g' /etc/opensips/opensips.cfg

## Run the following command for CX9 code only
rm -rf /etc/node/thirdparty/parser.js /etc/node/thirdparty/thirdparty.js
mv /etc/node/thirdparty/parser2.0.js  /etc/node/thirdparty/parser.js
mv /etc/node/thirdparty/thirdparty2.0.js /etc/node/thirdparty/thirdparty.js

## Setting the Config file under the /etc/node directory
# Change the following lines:
	"acl": [],
	"base_url": "https://{CHANGEME}/intellicon/",
	"cx9_base_url": "{CHANGEME}",

# Like this but as per your URL and IP-Address
# Must Write the 127.0.0.1 >>>> For more info refer to PM2 logs
	"acl": ["127.0.0.1","IPADDRESS","https://domain.com"],
	"base_url": "https://domain.com/intellicon/",
	"cx9_base_url": "domain.com",

# For Database need to set the Password Manually will be fixed soon in Script
# Run the following Commands to access the database and Dump the databases accordingly
chmod 755 db_setting && ./db_setting

# Password save in /root/my.cnf (if you set during installation process)
# Access the database with the following command
mysql -uroot --password='CHANGE-WITH-YOUR-GIVEN-PASSWORD'

# And then run the below command
set password for 'root'@'localhost' = password('CHANGE-WITH-YOUR-GIVEN-PASSWORD');
